#!/bin/bash -e

image=images/20201105_1610.raspbian-balenafin-shz.8g.img
device=$(ls /dev/disk/by-id/usb-RPi*:0) # This will find the mmc on the balenafin
block_size=$(sudo blockdev --getbsz ${device})

echo "will write ${image} to ${device}?"
read
sudo dd if=${image} of=${device} bs=${block_size} status=progress

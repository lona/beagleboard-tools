import os
import threading
from time import sleep
import signal

FIFO_PATH = "/tmp/lona_led0"

if not os.path.exists("/tmp/lona_led0"):
    os.mkfifo("/tmp/lona_led0")

class LedManager(threading.Thread):

    _mode: int = 0
    _running = True

    def on(self):
        print(f'0', end="\r")

    def off(self):
        print(f'-', end="\r")

    def _solid(self):
        self.on()
        sleep(0.5)

    def _blink(self, mode):
        self.on()
        sleep(1/mode)
        self.off()
        sleep(1/mode)

    def run(self):

        while self._running:

            if self._mode == 0:
                self._solid()
            else:
                self._blink(self._mode)

    def set_mode(self, c: str):
        parse_c = 0
        try:
            parse_c = int.from_bytes(c, 'big')
        except:
            raise
            pass

        self._mode = parse_c

    def stop(self):
        self._running = False

class NamedPipeManager(threading.Thread):

    _running = True

    def __init__(self, handle_char):
        self._handle_byte = handle_char

        super().__init__()


    def run(self):

        while self._running:

            d = os.open('/tmp/lona_led0', os.O_RDONLY)
            c = os.read(d, 1)

            os.close(d)

            self._handle_byte(c)

    def stop(self):
        self._running = False

        d = os.open('/tmp/lona_led0', os.O_WRONLY)
        c = os.write(d, b'0')
        os.close(d)

if __name__ == "__main__":

    blinker = LedManager()
    blinker.start()

    NamedPipeManager = NamedPipeManager(blinker.set_mode)
    NamedPipeManager.start()


    def shutdown_handler(sig, sf):
        blinker.stop()
        NamedPipeManager.stop()
        pass

    signal.signal(signal.SIGTERM, shutdown_handler)
    signal.signal(signal.SIGINT, shutdown_handler)



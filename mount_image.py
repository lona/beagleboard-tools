#!/usr/bin/env python3

import subprocess
import re
import os
import sys

reldir = os.path.dirname(os.path.realpath(__file__))
mountdir = os.path.join(reldir, 'img_mounts')

image = sys.argv[1]
image_basename = os.path.basename(image)

cmd = f"""parted -s {image} unit b print"""

result = subprocess.run(cmd, shell=True, capture_output=True, check=True)
options = []

for line in result.stdout.decode('utf8').split('\n'):

    if re.match('^ [0-9]', line):

        fields = re.findall('\s+?(\S+)', line)

        fstype = '?'
        if len(fields) == 6:
            fstype = fields[5]

        options.append({
            'number': fields[0],
            'start': fields[1].replace("B", ""),
            'size_mb': int(fields[3] .replace("B", "")) / (1024*1024),
            'fstype': fstype
        })

for o in options:
    print(f"P{o['number']} {o['size_mb']:.2f}M {o['fstype']}")

choice = int(input('Select partition # to mount: '))

mount_choice = options[choice-1]

image_mount_dir = os.path.join(mountdir, image_basename, mount_choice['number'])

os.makedirs(image_mount_dir, exist_ok=True)
cmd = f"sudo mount -o loop,offset={mount_choice['start']} {image} {image_mount_dir}"

print(cmd)
subprocess.run(cmd, shell=True, capture_output=True, check=True)
print(f"Mounted at {image_mount_dir}")


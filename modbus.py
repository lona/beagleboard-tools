import minimalmodbus
import serial

tag_reader = minimalmodbus.Instrument('/dev/ttyUSB1', 2, debug=True)
tag_reader.serial.parity=serial.PARITY_ODD
tag_reader.serial.baudrate=19200



emit_on_tag_read='0'
antenna_open = '1'
beeper = '0'

tag_reader.write_register(0, int(f'{beeper}{antenna_open}{emit_on_tag_read}', 2), functioncode=6)


# scan()
# result = tag_reader.read_registers(5, 17, functioncode=3)

def x(r):
    hex = "{:04x}".format(r)
    return hex[0:2] + ' ' + hex[2:4]

from time import sleep

def registers_to_bytes(registers):

    byte_array = []

    # Split up each 16 bit register into bytes
    for i in registers:
        byte_array.append(i >> 8)
        byte_array.append(i & 255)

    return byte_array


def query_reader():

    # should be 5 16-bit registers
    result_registers = tag_reader.read_registers(int('1B', 16), int('5', 16))

    mask = int('FF00', 16)
    time = result_registers[0]

    result_bytes = registers_to_bytes(result_registers)

    time = (result_bytes[0]/255)*5
    strength = result_bytes[1]
    countrycode = result_registers[1] & int('0FFF', 16)
    tag = int.from_bytes(result_bytes[4:9], "big", signed=False)

    return (f'{countrycode}{tag:0>12}', time, strength)


while True:
    (tag, time, strength) = query_reader()

    if(time >= 5):
        gt = '>'
    else:
        gt = ' '

    print(f'{tag} [{gt} {time:.2f} sec] [Strength: {strength}]')
    sleep(0.1)
#!/bin/bash

# This tries to find a mounted raspberry pi compute module and copy the image off of it.
# You may need this: https://github.com/raspberrypi/usbboot

set -e

image_name='raspbian-balenafin-shz'

# Find Balenafin.. you may need to run rpiusbboot to make this appear.
device=$(ls /dev/disk/by-id/usb-RPi*:0)

output_file=./images/$(date +%Y%m%d_%H%M).${image_name}.8g.img
block_size=$(sudo blockdev --getbsz ${device})
total_bytes=$(expr 512 \* $(sudo blockdev  --getsz ${device} ))
total_gb=$(expr ${total_bytes} / 1073741824)
count=$(expr ${total_bytes} / ${block_size})

echo Copying ~${total_gb}gb block device ${device} to ${output_file}?
read

sudo dd if=${device} bs=${block_size} count=${count} status=progress > ${output_file}

# sudo dd if=${device} bs=${block_size} count=${count} status=progress | \
#     xz -z --stdout > ${output_file}

#!/bin/bash -e

SOURCE=https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tgz
SHA256SUM=d63e63e14e6d29e17490abbe6f7d17afb3db182dbd801229f14e55f4157c4ba3

workdir=/tmp/python37-work
archive_dest=${workdir}/$(basename ${SOURCE})

package_name=$(basename ${SOURCE} .tgz| tr '[:upper:]' '[:lower:]')

function install-deps () {
  BUILD_DEPS="curl build-essential libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev"
  apt-get install -y ${BUILD_DEPS}
}

function compile-python () {

  mkdir -p ${workdir}

  test ! -e "${archive_dest}" && curl -o ${archive_dest} "${SOURCE}"

  echo "${SHA256SUM}  ${archive_dest}" > ${workdir}/sha256sum

  sha256sum -c ${workdir}/sha256sum || exit 1

  tar -xf ${archive_dest} -C ${workdir} --strip=1

  cd ${workdir}
  ./configure --enable-optimizations --prefix=/opt/${package_name}
  make
  make install
}

function package-python () {
  tar -czf ${archive_dest}/${package_name}-Debian$(cat /etc/debian_version)-$(uname -m).tgz -C /opt /opt/${package_name}
}

$1


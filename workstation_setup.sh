
function add_to_group () {
  if grep $1 -q /etc/group; then
    echo Adding ${USER} to ${1}
    # Allow current user to access tty devices
    sudo usermod -a -G $1 ${USER}
  else
    echo Adding ${USER} to ${1}
  fi
}

add_to_group uucp
add_to_group dialout



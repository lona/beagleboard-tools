#!/bin/bash

dirname=$(dirname "$0")

ansible-playbook -u pi --become -kK  -e @$dirname/playbooks/vars/main.yml $@

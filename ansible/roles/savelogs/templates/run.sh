#!/bin/bash -e
# Should run on shutdown to save the logs to good storage.

SAVELOGS_DEST={{savelogs_dest}}

fullpath=${SAVELOGS_DEST}/$(date +%s).syslog.log.gz

mkdir -p ${SAVELOGS_DEST}
journalctl -b | gzip > ${fullpath}

if test -x {{shz_artifact_dest}}/backup.sh; then
    {{shz_artifact_dest}}/backup.sh
fi
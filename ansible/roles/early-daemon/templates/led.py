#!/usr/bin/env python3

from gpiozero import LED
from time import sleep
import signal

led1 = LED(23)
led2 = LED(24)

def fn(*args):
    led1.off()
    led2.off()
    del led1
    del led2

signal.signal(signal.SIGTERM, fn)
signal.signal(signal.SIGINT, fn)

while True:
    led1.on()
    led2.on()
    sleep(0.5)




#!/bin/bash -e

export ARTIFACT_DESTINATION={{shz_artifact_dest}}

get_artifacts () {
    ls -1 {{shz_extdata_mountpoint}}/shz*.tgz
}

# If there is an artifact... load it.
if get_artifacts &> /dev/null; then

    artifact=$(get_artifacts | head -n 1)
    echo "Found artifact at ${artifact}... Extracting to ${ARTIFACT_DESTINATION}"

    rm -rf ${ARTIFACT_DESTINATION}
    mkdir -p ${ARTIFACT_DESTINATION}

    tar -xf ${artifact} -C ${ARTIFACT_DESTINATION}
    mkdir -p ${DATA_DIRECTORY}/loaded_artifacts
    mv ${artifact} ${DATA_DIRECTORY}/loaded_artifacts
fi

cd ${ARTIFACT_DIRECTORY}

# Todo: generalize to a __main__ module invocation of sorts?
/usr/bin/python3 -m shz.app